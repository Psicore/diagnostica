/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entity.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author militza.landaverdeus
 */
public class ClienteDao extends Conexion.Conexion {

    public List<Cliente> llenarCliente() throws Exception {
        List<Cliente> lista = new ArrayList<Cliente>();
        try {
            this.Conectar();

            String query = "select * from cliente";
            PreparedStatement stm = this.getCnx().prepareStatement(query);
            ResultSet rt = stm.executeQuery();

            while (rt.next()) {
                Cliente c = new Cliente();

                c.setId_cliente(rt.getInt("id_cliente"));
                c.setNombre(rt.getString("nombre"));
                c.setApellido(rt.getString("apellido"));
                c.setDireccion(rt.getString("direccion"));
                c.setTelefono(rt.getInt("telefono"));
                c.setFecha_nacimiento(rt.getDate("fecha_nacimiento"));

                lista.add(c);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Desconectar();
        }
        return lista;
    }

}
