/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entity.Cliente;
import entity.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author militza.landaverdeus
 */
public class ProductoDao extends Conexion.Conexion{
    public List<Producto> llenarProducto() throws Exception {
        List<Producto> lista = new ArrayList<Producto>();
        try {
            this.Conectar();

            String query = "select * from producto";
            PreparedStatement stm = this.getCnx().prepareStatement(query);
            ResultSet rt = stm.executeQuery();

            while (rt.next()) {
                Producto p = new Producto();

                p.setId_producto(rt.getInt("id_producto"));
                p.setNombre(rt.getString("nombre"));
                p.setPrecio(rt.getDouble("precio"));
                p.setStock(rt.getInt("stock"));

                lista.add(p);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Desconectar();
        }
        return lista;
    }
}
