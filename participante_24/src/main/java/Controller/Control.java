/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ClienteDao;
import DAO.ProductoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author militza.landaverdeus
 */
@WebServlet(name = "Control", urlPatterns = {"/Control"})
public class Control extends HttpServlet {

    ClienteDao cliente;
    ProductoDao producto;

    public Control() {
        super();
        cliente = new ClienteDao();
        producto = new ProductoDao();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("lsc")) {
            ruta = "/cliente.jsp";
        }else 
        if (action.equalsIgnoreCase("lsp")) {
            ruta = "/producto.jsp";
        }

        RequestDispatcher rt = request.getRequestDispatcher(ruta);
        rt.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
