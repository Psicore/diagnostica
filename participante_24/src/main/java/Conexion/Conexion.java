/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.io.Serializable;
import java.sql.DriverManager;

/**
 *
 * @author militza.landaverdeus
 */
public class Conexion{
    
    private String bd = "participante_24";
    private String pass = "root";
    private String user = "root";
    private String ruta = "jdbc:mysql://localhost/" + bd;
    
    Connection cnx;

    public Connection getCnx() {
        return cnx;
    }

    public void setCnx(Connection cnx) {
        this.cnx = cnx;
    }
    
    public void Conectar() throws Exception{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.cnx = DriverManager.getConnection(ruta, user, pass);
        } catch (Exception e) {
            throw e;
        }
    }
    
    
    public void Desconectar(){
        try {
            if(this.cnx != null){
                if(this.cnx.isClosed() != true){
                    this.cnx.close();
                }
            }
        } catch (Exception e) {
        }
    }
}
